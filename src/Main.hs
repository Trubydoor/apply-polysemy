{-# OPTIONS_GHC -fplugin=Polysemy.Plugin #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE LambdaCase, BlockArguments, ScopedTypeVariables #-}
{-# LANGUAGE GADTs, FlexibleContexts, TypeOperators, DataKinds, PolyKinds #-}
module Main where

import Polysemy
import Polysemy.State
import Polysemy.AtomicState
import GHC.Conc

data Stuff = Foo | Bar | Baz

data Applier m a where
  Apply :: Stuff -> Applier m ()

makeSem ''Applier

applierToState :: Sem (Applier : r) a -> Sem (State Int : r) a
applierToState = reinterpret $ \case
  Apply stuff -> case stuff of
    Foo -> modify (+1)
    Bar -> modify (+2)
    Baz -> modify (+4)

applierToAtomic :: Sem (Applier : r) a -> Sem (AtomicState Int : r) a
applierToAtomic = reinterpret $ \case
  Apply stuff -> case stuff of
    Foo -> atomicModify' (+1)
    Bar -> atomicModify' (+2)
    Baz -> atomicModify' (+4)

runApply :: Sem '[Applier] () -> Int
runApply = fst . run . runState 0 . applierToState

execAtomicState :: Member (Embed IO) r => s -> Sem (AtomicState s ': r) a -> Sem r s
execAtomicState s m = fmap fst $ atomicStateToIO s m

runApplyAtomic :: Sem '[Applier, Embed IO] () -> IO Int
runApplyAtomic = runM . execAtomicState 0 . applierToAtomic

runStuffAtomic :: IO Int
runStuffAtomic = runApplyAtomic $ do
  apply Foo
  apply Bar
  apply Baz

runStuff :: Int
runStuff = runApply $ do
  apply Foo
  apply Bar
  apply Baz

main :: IO ()
main = runStuffAtomic >>= print
